EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "MainBoard"
Date "2022-09-03"
Rev "V_1"
Comp "Embedsa"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6650 2350 2000 3350
U 606727AA
F0 "mainboard" 50
F1 "mainboard.sch" 50
F2 "VDC" I L 6650 2500 79 
F3 "GNDDC" I L 6650 3000 79 
$EndSheet
$Comp
L Monitor-Dps9000-rescue:Conn_01x02-Connector_Generic J?
U 1 1 6106891B
P 4050 2600
AR Path="/6031687A/6106891B" Ref="J?"  Part="1" 
AR Path="/6106891B" Ref="J1"  Part="1" 
F 0 "J1" H 4100 2817 50  0000 C CNN
F 1 "Conn_02x02_Counter_Clockwise" H 4100 2726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4050 2600 50  0001 C CNN
F 3 "~" H 4050 2600 50  0001 C CNN
	1    4050 2600
	-1   0    0    1   
$EndComp
Text GLabel 4250 2500 2    50   Input ~ 0
DC1
Text GLabel 4250 2600 2    50   Input ~ 0
GND-DC2
Wire Wire Line
	4250 2500 6650 2500
Wire Wire Line
	4250 3000 6650 3000
Wire Wire Line
	4250 3000 4250 2600
$EndSCHEMATC
