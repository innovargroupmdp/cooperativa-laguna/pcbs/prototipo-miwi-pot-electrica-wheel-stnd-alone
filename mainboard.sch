EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "MainBoard"
Date "2022-09-03"
Rev "V_1"
Comp "Embedsa"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Monitor-Dps9000-rescue:PIC18LF26J50_SO-Library-cbas U4
U 1 1 60562A41
P 2650 5150
F 0 "U4" H 2650 6331 50  0000 C CNN
F 1 "PIC18LF26J50_SO" H 2650 6240 50  0000 C CNN
F 2 "Package_SO:SSOP-28_5.3x10.2mm_P0.65mm" H 2550 6250 100 0001 C CNN
F 3 "" H 2650 5100 50  0001 C CNN
	1    2650 5150
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:Mrf24j40ma-Library-cbas U6
U 1 1 6057AB80
P 8650 4250
F 0 "U6" H 8675 4375 50  0000 C CNN
F 1 "Mrf24j40ma" H 8675 4284 50  0000 C CNN
F 2 "Monitor-RF_MODULE:microchip_MRF24F_SMD" H 8700 4250 50  0001 C CNN
F 3 "" H 8700 4250 50  0001 C CNN
	1    8650 4250
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:Apds-9930-Library-cbas U?
U 1 1 6056F2C2
P 5400 1650
AR Path="/6056F2C2" Ref="U?"  Part="1" 
AR Path="/606727AA/6056F2C2" Ref="U5"  Part="1" 
F 0 "U5" H 5400 1775 50  0000 C CNN
F 1 "Apds-9930" H 5400 1684 50  0000 C CNN
F 2 "Monitor-RF_MODULE:apds-9930-qga_8" H 5450 1600 50  0001 C CNN
F 3 "" H 5450 1600 50  0001 C CNN
	1    5400 1650
	1    0    0    -1  
$EndComp
Text HLabel 2600 2000 0    50   Input ~ 0
VDC
Text HLabel 2600 2450 0    50   Input ~ 0
GNDDC
Wire Wire Line
	2600 2450 2900 2450
$Comp
L Monitor-Dps9000-rescue:CP-Device C14
U 1 1 60589042
P 9600 4800
F 0 "C14" H 9718 4846 50  0000 L CNN
F 1 "1u" H 9718 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9638 4650 50  0001 C CNN
F 3 "~" H 9600 4800 50  0001 C CNN
	1    9600 4800
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:CP-Device C15
U 1 1 6058A48C
P 10050 4800
F 0 "C15" H 10168 4846 50  0000 L CNN
F 1 "0.1u" H 10168 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10088 4650 50  0001 C CNN
F 3 "~" H 10050 4800 50  0001 C CNN
	1    10050 4800
	1    0    0    -1  
$EndComp
Text GLabel 2750 2000 2    50   Input ~ 0
Vcc
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR03
U 1 1 6058C443
P 2900 2450
F 0 "#PWR03" H 2900 2200 50  0001 C CNN
F 1 "GND" H 2905 2277 50  0000 C CNN
F 2 "" H 2900 2450 50  0001 C CNN
F 3 "" H 2900 2450 50  0001 C CNN
	1    2900 2450
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR02
U 1 1 6058D673
P 2500 6200
F 0 "#PWR02" H 2500 5950 50  0001 C CNN
F 1 "GND" H 2505 6027 50  0000 C CNN
F 2 "" H 2500 6200 50  0001 C CNN
F 3 "" H 2500 6200 50  0001 C CNN
	1    2500 6200
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR04
U 1 1 6058E73F
P 7750 4350
F 0 "#PWR04" H 7750 4100 50  0001 C CNN
F 1 "GND" H 7755 4177 50  0000 C CNN
F 2 "" H 7750 4350 50  0001 C CNN
F 3 "" H 7750 4350 50  0001 C CNN
	1    7750 4350
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR06
U 1 1 6058EFE4
P 9350 4350
F 0 "#PWR06" H 9350 4100 50  0001 C CNN
F 1 "GND" H 9355 4177 50  0000 C CNN
F 2 "" H 9350 4350 50  0001 C CNN
F 3 "" H 9350 4350 50  0001 C CNN
	1    9350 4350
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR05
U 1 1 6058FE72
P 6350 2050
F 0 "#PWR05" H 6350 1800 50  0001 C CNN
F 1 "GND" H 6355 1877 50  0000 C CNN
F 2 "" H 6350 2050 50  0001 C CNN
F 3 "" H 6350 2050 50  0001 C CNN
	1    6350 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 4350 7750 4350
Wire Wire Line
	9100 4350 9200 4350
Wire Wire Line
	9100 4500 9200 4500
Wire Wire Line
	9200 4500 9200 4350
Connection ~ 9200 4350
Wire Wire Line
	9200 4350 9350 4350
Wire Wire Line
	9100 4650 9600 4650
Wire Wire Line
	9600 4650 10050 4650
Connection ~ 9600 4650
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR07
U 1 1 6059350E
P 9600 5100
F 0 "#PWR07" H 9600 4850 50  0001 C CNN
F 1 "GND" H 9605 4927 50  0000 C CNN
F 2 "" H 9600 5100 50  0001 C CNN
F 3 "" H 9600 5100 50  0001 C CNN
	1    9600 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 5100 9600 4950
Wire Wire Line
	9600 4950 10050 4950
Connection ~ 9600 4950
Wire Wire Line
	2450 6150 2500 6150
Wire Wire Line
	2500 6200 2500 6150
Connection ~ 2500 6150
Wire Wire Line
	2500 6150 2550 6150
$Comp
L Monitor-Dps9000-rescue:CP-Device C11
U 1 1 60597719
P 2200 3800
F 0 "C11" H 2318 3846 50  0000 L CNN
F 1 "0.047u" H 2318 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2238 3650 50  0001 C CNN
F 3 "~" H 2200 3800 50  0001 C CNN
	1    2200 3800
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:CP-Device C10
U 1 1 6059826D
P 1900 3800
F 0 "C10" H 2018 3846 50  0000 L CNN
F 1 "0.1u" H 2018 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1938 3650 50  0001 C CNN
F 3 "~" H 1900 3800 50  0001 C CNN
	1    1900 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3650 2200 3650
Wire Wire Line
	2200 3650 2550 3650
Wire Wire Line
	2550 3650 2550 4150
Connection ~ 2200 3650
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR01
U 1 1 60599284
P 1900 4000
F 0 "#PWR01" H 1900 3750 50  0001 C CNN
F 1 "GND" H 1905 3827 50  0000 C CNN
F 2 "" H 1900 4000 50  0001 C CNN
F 3 "" H 1900 4000 50  0001 C CNN
	1    1900 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 4000 1900 3950
Wire Wire Line
	2200 3950 1900 3950
Connection ~ 1900 3950
$Comp
L Monitor-Dps9000-rescue:CP-Device C13
U 1 1 605A1160
P 6350 1900
F 0 "C13" H 6468 1946 50  0000 L CNN
F 1 "CP" H 6468 1855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6388 1750 50  0001 C CNN
F 3 "~" H 6350 1900 50  0001 C CNN
	1    6350 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1750 5800 1750
Text GLabel 6650 1750 2    50   Input ~ 0
Vcc
Text GLabel 10350 4650 2    50   Input ~ 0
Vcc
Text GLabel 2850 3650 2    50   Input ~ 0
Vcc
Text GLabel 1050 4750 0    50   Input ~ 0
Vcc
Wire Wire Line
	2850 3650 2550 3650
Connection ~ 2550 3650
Wire Wire Line
	6650 1750 6350 1750
Connection ~ 6350 1750
Wire Wire Line
	10350 4650 10050 4650
Connection ~ 10050 4650
$Comp
L Monitor-Dps9000-rescue:DIODE-pspice D5
U 1 1 605AC002
P 1600 4750
F 0 "D5" H 1600 5015 50  0000 C CNN
F 1 "DIODE" H 1600 4924 50  0000 C CNN
F 2 "Diode_SMD:D_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1600 4750 50  0001 C CNN
F 3 "~" H 1600 4750 50  0001 C CNN
	1    1600 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 4750 1250 4750
Wire Wire Line
	1800 4750 1850 4750
Wire Wire Line
	5800 1950 5950 1950
Wire Wire Line
	5950 1950 5950 2050
$Comp
L Monitor-Dps9000-rescue:25AA02E-OT U2
U 1 1 60F54BF8
P 5400 4700
F 0 "U2" H 5400 5181 50  0000 C CNN
F 1 "25AA02E-OT" H 5400 5090 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 5400 5200 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21709J.pdf" H 5400 4700 50  0001 C CNN
	1    5400 4700
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:Crystal-Device Y1
U 1 1 60F595AB
P 1700 6900
F 0 "Y1" H 1700 7168 50  0000 C CNN
F 1 "Crystal" H 1700 7077 50  0000 C CNN
F 2 "Crystal:Crystal_C26-LF_D2.1mm_L6.5mm_Vertical" H 1700 6900 50  0001 C CNN
F 3 "~" H 1700 6900 50  0001 C CNN
	1    1700 6900
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:CP-Device C17
U 1 1 60F5A5B0
P 1950 7200
F 0 "C17" H 2068 7246 50  0000 L CNN
F 1 "15p" H 2068 7155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1988 7050 50  0001 C CNN
F 3 "~" H 1950 7200 50  0001 C CNN
	1    1950 7200
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:CP-Device C16
U 1 1 60F5ACAE
P 1500 7200
F 0 "C16" H 1618 7246 50  0000 L CNN
F 1 "15p" H 1618 7155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1538 7050 50  0001 C CNN
F 3 "~" H 1500 7200 50  0001 C CNN
	1    1500 7200
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR0101
U 1 1 60F5CE01
P 1500 7350
F 0 "#PWR0101" H 1500 7100 50  0001 C CNN
F 1 "GND" H 1505 7177 50  0000 C CNN
F 2 "" H 1500 7350 50  0001 C CNN
F 3 "" H 1500 7350 50  0001 C CNN
	1    1500 7350
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR0102
U 1 1 60F5D274
P 1950 7350
F 0 "#PWR0102" H 1950 7100 50  0001 C CNN
F 1 "GND" H 1955 7177 50  0000 C CNN
F 2 "" H 1950 7350 50  0001 C CNN
F 3 "" H 1950 7350 50  0001 C CNN
	1    1950 7350
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR0103
U 1 1 60F5E8B7
P 5400 5000
F 0 "#PWR0103" H 5400 4750 50  0001 C CNN
F 1 "GND" H 5405 4827 50  0000 C CNN
F 2 "" H 5400 5000 50  0001 C CNN
F 3 "" H 5400 5000 50  0001 C CNN
	1    5400 5000
	1    0    0    -1  
$EndComp
Text GLabel 5400 4400 2    50   Input ~ 0
Vcc
Wire Wire Line
	1500 7050 1500 6900
Wire Wire Line
	1500 6900 1550 6900
Wire Wire Line
	1850 6900 1950 6900
Wire Wire Line
	1950 6900 1950 7050
Text GLabel 3600 5250 2    50   Input ~ 0
os0
Text GLabel 3600 5350 2    50   Input ~ 0
os1
Wire Wire Line
	3450 5250 3600 5250
Wire Wire Line
	3600 5350 3450 5350
Text GLabel 1350 6900 0    50   Input ~ 0
os0
Text GLabel 2100 6900 2    50   Input ~ 0
os1
Wire Wire Line
	1500 6900 1350 6900
Wire Wire Line
	2100 6900 1950 6900
Connection ~ 1950 6900
Connection ~ 1500 6900
Text GLabel 3600 4350 2    50   Input ~ 0
INT0
Wire Wire Line
	3450 4350 3600 4350
Text GLabel 3600 4650 2    50   Input ~ 0
SCK
Wire Wire Line
	3450 4750 3600 4750
Text GLabel 3600 4450 2    50   Input ~ 0
SDI
Wire Wire Line
	3450 4850 3600 4850
Text GLabel 3600 4950 2    50   Input ~ 0
Pgc
Text GLabel 3600 5050 2    50   Input ~ 0
Pgd
Wire Wire Line
	3450 5050 3600 5050
Text GLabel 3600 5650 2    50   Input ~ 0
CS
Wire Wire Line
	3450 5650 3600 5650
Text GLabel 3600 5850 2    50   Input ~ 0
nRST
Wire Wire Line
	3450 5750 3600 5750
Text GLabel 3600 5950 2    50   Input ~ 0
WK
Wire Wire Line
	3450 5850 3600 5850
Text GLabel 3600 4550 2    50   Input ~ 0
SDO
Wire Wire Line
	3450 5950 3600 5950
Text GLabel 8100 4800 0    50   Input ~ 0
INT0
Wire Wire Line
	8250 4800 8100 4800
Text GLabel 8100 5100 0    50   Input ~ 0
SCK
Wire Wire Line
	8250 5100 8100 5100
Text GLabel 9250 5100 2    50   Input ~ 0
SDI
Wire Wire Line
	9100 5100 9250 5100
Text GLabel 8100 4950 0    50   Input ~ 0
SDO
Wire Wire Line
	8250 4950 8100 4950
Text GLabel 8100 4650 0    50   Input ~ 0
WK
Wire Wire Line
	8250 4650 8100 4650
Text GLabel 9250 4950 2    50   Input ~ 0
CS
Wire Wire Line
	9100 4950 9250 4950
Text GLabel 8100 4500 0    50   Input ~ 0
nRST
Wire Wire Line
	8250 4500 8100 4500
$Comp
L Monitor-Dps9000-rescue:R-Device R11
U 1 1 60FD87D6
P 1250 4900
F 0 "R11" H 1050 4950 50  0000 L CNN
F 1 "10k" H 1050 4850 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1180 4900 50  0001 C CNN
F 3 "~" H 1250 4900 50  0001 C CNN
	1    1250 4900
	1    0    0    -1  
$EndComp
Connection ~ 1250 4750
Wire Wire Line
	1250 4750 1400 4750
Text GLabel 1050 5050 0    50   Input ~ 0
Vpp
Wire Wire Line
	1250 5050 1050 5050
Connection ~ 1250 5050
Wire Wire Line
	1250 5050 1850 5050
Text GLabel 5800 4600 2    50   Input ~ 0
SDO
Text GLabel 5000 4700 0    50   Input ~ 0
SDI
Text GLabel 5800 4700 2    50   Input ~ 0
SCK
Text GLabel 6000 5250 2    50   Input ~ 0
ee_CS
Text GLabel 3900 4950 2    50   Input ~ 0
ee_CS
Wire Wire Line
	3450 4450 3600 4450
NoConn ~ 3450 5450
NoConn ~ 3450 5550
NoConn ~ 1850 5350
NoConn ~ 1850 5250
NoConn ~ 1850 4850
NoConn ~ 1850 4650
NoConn ~ 1850 4550
NoConn ~ 1850 4450
NoConn ~ 1850 4350
Text GLabel 4750 1550 0    50   Input ~ 0
I2C_DA
Text GLabel 6000 1550 2    50   Input ~ 0
I2C_CL
Wire Wire Line
	5800 1850 5850 1850
NoConn ~ 5800 2050
NoConn ~ 5000 2050
NoConn ~ 5000 1950
NoConn ~ 5000 1850
$Comp
L Monitor-Dps9000-rescue:Conn_01x05_Male-Connector J3
U 1 1 60FAD837
P 5500 7200
F 0 "J3" H 5608 7581 50  0000 C CNN
F 1 "Conn_01x05_Male" H 5608 7490 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x05_P1.27mm_Vertical" H 5500 7200 50  0001 C CNN
F 3 "~" H 5500 7200 50  0001 C CNN
	1    5500 7200
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR0104
U 1 1 60FAED67
P 5700 7400
F 0 "#PWR0104" H 5700 7150 50  0001 C CNN
F 1 "GND" H 5705 7227 50  0000 C CNN
F 2 "" H 5700 7400 50  0001 C CNN
F 3 "" H 5700 7400 50  0001 C CNN
	1    5700 7400
	1    0    0    -1  
$EndComp
Text GLabel 5700 7300 2    50   Input ~ 0
Vcc
Text GLabel 5700 7200 2    50   Input ~ 0
Pgc
Text GLabel 5700 7100 2    50   Input ~ 0
Pgd
Text GLabel 5700 7000 2    50   Input ~ 0
Vpp
Text GLabel 3600 4850 2    50   Input ~ 0
I2C_DA
Text GLabel 3600 4750 2    50   Input ~ 0
I2C_CL
Wire Wire Line
	3600 4550 3450 4550
Wire Wire Line
	3600 4650 3450 4650
Wire Wire Line
	3450 4950 3900 4950
Text GLabel 3600 5750 2    50   Input ~ 0
LED
$Comp
L Monitor-Dps9000-rescue:R-Device R12
U 1 1 6101B445
P 6000 4950
F 0 "R12" H 6070 4996 50  0000 L CNN
F 1 "100" H 6070 4905 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5930 4950 50  0001 C CNN
F 3 "~" H 6000 4950 50  0001 C CNN
	1    6000 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4800 5800 4800
$Comp
L Monitor-Dps9000-rescue:LED-Device D7
U 1 1 6102F689
P 3800 7150
F 0 "D7" V 3747 7230 50  0000 L CNN
F 1 "LED" V 3838 7230 50  0000 L CNN
F 2 "LED_SMD:LED_0603_1608Metric_Castellated" H 3800 7150 50  0001 C CNN
F 3 "~" H 3800 7150 50  0001 C CNN
	1    3800 7150
	0    1    1    0   
$EndComp
Text GLabel 3800 7300 2    50   Input ~ 0
Vcc
Text GLabel 3800 7000 2    50   Input ~ 0
LED
$Comp
L Monitor-Dps9000-rescue:Conn_01x04_Male-Connector J4
U 1 1 60F48E5B
P 8250 1900
AR Path="/60F48E5B" Ref="J4"  Part="1" 
AR Path="/606727AA/60F48E5B" Ref="J4"  Part="1" 
F 0 "J4" H 8358 2281 50  0000 C CNN
F 1 "Conn_01x04_Male" H 8358 2190 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical" H 8250 1900 50  0001 C CNN
F 3 "~" H 8250 1900 50  0001 C CNN
	1    8250 1900
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR0105
U 1 1 60F4BCDF
P 8450 2100
F 0 "#PWR0105" H 8450 1850 50  0001 C CNN
F 1 "GND" H 8455 1927 50  0000 C CNN
F 2 "" H 8450 2100 50  0001 C CNN
F 3 "" H 8450 2100 50  0001 C CNN
	1    8450 2100
	1    0    0    -1  
$EndComp
Text GLabel 8450 2000 2    50   Input ~ 0
Vcc
Text GLabel 8450 1800 2    50   Input ~ 0
I2C_CL
Text GLabel 8450 1900 2    50   Input ~ 0
I2C_DA
$Comp
L Monitor-Dps9000-rescue:R-Device R14
U 1 1 60F4B2CB
P 5850 1350
F 0 "R14" H 5920 1396 50  0000 L CNN
F 1 "10k" H 5920 1305 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 5780 1350 50  0001 C CNN
F 3 "~" H 5850 1350 50  0001 C CNN
	1    5850 1350
	-1   0    0    1   
$EndComp
Connection ~ 5850 1550
$Comp
L Monitor-Dps9000-rescue:R-Device R13
U 1 1 60F4D9F9
P 4900 1350
F 0 "R13" H 4970 1396 50  0000 L CNN
F 1 "10k" H 4970 1305 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4830 1350 50  0001 C CNN
F 3 "~" H 4900 1350 50  0001 C CNN
	1    4900 1350
	1    0    0    1   
$EndComp
Text GLabel 5850 1100 2    50   Input ~ 0
Vcc
Text GLabel 4900 1100 2    50   Input ~ 0
Vcc
Wire Wire Line
	5850 1850 5850 1550
Wire Wire Line
	4900 1750 5000 1750
Wire Wire Line
	2600 2000 2750 2000
Text Notes 8900 2200 0    118  ~ 0
Bus\nDatos\nExternos
Text Notes 6150 7400 0    118  ~ 0
Bus\nICSP
Wire Wire Line
	6000 5250 6000 5100
Wire Wire Line
	5850 1200 5850 1100
Wire Wire Line
	5850 1550 6000 1550
Wire Wire Line
	5850 1550 5850 1500
Wire Wire Line
	4900 1500 4900 1550
Wire Wire Line
	4750 1550 4900 1550
Connection ~ 4900 1550
Wire Wire Line
	4900 1550 4900 1750
Wire Wire Line
	4900 1200 4900 1100
$Comp
L Monitor-Dps9000-rescue:GND-power #PWR?
U 1 1 63238395
P 5950 2050
F 0 "#PWR?" H 5950 1800 50  0001 C CNN
F 1 "GND" H 5955 1877 50  0000 C CNN
F 2 "" H 5950 2050 50  0001 C CNN
F 3 "" H 5950 2050 50  0001 C CNN
	1    5950 2050
	1    0    0    -1  
$EndComp
Wire Notes Line
	10000 2800 10000 750 
Wire Notes Line
	4050 750  4050 2800
Wire Notes Line
	4050 2800 10000 2800
Wire Notes Line
	10000 750  4050 750 
Text Notes 6200 1050 0    118  ~ 24
SENSOR EXTERNO
Wire Notes Line
	1700 1000 1700 2800
Wire Notes Line
	3650 2850 3650 1000
Wire Notes Line
	3650 1000 1700 1000
Wire Notes Line
	1700 2800 3650 2800
Text Notes 2150 1650 0    118  ~ 24
ENTRADA\nALIMENTACION\nREGULADA
Text Notes 6350 5050 0    118  ~ 0
Bus\nDatos\nInterno
Wire Notes Line
	4500 3500 4500 5500
Wire Notes Line
	4500 5500 11000 5500
Wire Notes Line
	11000 5500 11000 3500
Wire Notes Line
	11000 3500 4500 3500
Text Notes 1550 3250 0    118  ~ 24
MICROCOMPUTADORA
Text Notes 8150 3900 0    118  ~ 24
TRASCEPTOR
Wire Notes Line
	7250 5500 7250 3500
Wire Notes Line
	750  6450 4350 6450
Wire Notes Line
	4350 6200 4350 3050
Wire Notes Line
	4350 2900 750  2900
Wire Notes Line
	750  3100 750  6250
$EndSCHEMATC
